# FYC Watson TP

L'objectif de ce TP est de créer un chatbot utilisant l'IA Watson pour aider les utilisateurs rencontrant des problèmes de sécurité à les résoudre.

Pour ce TP nous allons utiliser l'API de Watson Assistant.

## 1. Création du ChatBot

### 1.1 Créer son compte sur IBM Cloud

Vous devez créer un compte IBM Cloud pour récuperer une clef d'API Watson Assistant.

Pour créer votre compte rendez vous sur ce lien: https://cloud.ibm.com/registration/trial

### 1.2 Créer votre Watson Assistant

Depuis l'onglet catalogue de IBM Cloud, trouver et créer votre propre instance de Watson Assistant.

### 1.3 Lancer et configurer votre Watson Assistant

Vous avez désormais accès à votre instance de Watson Assistant et de sa clef API.

Lancer votre instance de Watson Assistant en cliquant sur le bouton "Lancer Watson Assistant".

Il ne vous reste plus qu'a créer votre Assistant et configurer ses skills. 

### 1.4 Configurer les skills

Créer maintenant un skill "Security" pour votre Assistant.

Dans ce skill, vous créerez des entités et des intents et des dialogues pour pouvoir répondre aux questions d'un utilisateurs sur au moin 2 problèmes de sécurité de votre choix. (e.g. Fishing, Brute force, Injection SQL, etc…).

Pour vous aider, vous pouvez importer le "skill-Security.json" dans votre Assistant. Ce skill comporte 2 exemples pour les problèmes de sécurité concernant les SQL Injection et les XSS Attack.

## 2. Création de votre interface utilisateur

### 2.1 Le code

IBM propose plusieurs SDK pour aider les développeurs à créer leur applications avec les différents API qu'ils proposent.

Vous trouverez ici toutes les SDK mise par IBM à votre disposition.
https://github.com/watson-developer-cloud

Créer une interface dans le langage de votre choix pour qu'un utilisateur puisse interagir avec votre chatbot.

## Correction 

Vous pouvez trouver dans le main.js un code d'exemple pour echanger avec votre chatbot. Remplacer juste dans le fichier les variables "apiKey" et "assistantId" par vos clés d'API et assistantId respectifs.

Vous n'avez plus qu'à lancer ces 2 commandes et à disctuter avec Watson: 

> npm install

> npm run start
