const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');
const { exit } = require('process');

// Replace this information with your own
const assistantId = "YOUR_ASSISTANT_ID";
const apikey = "YOUR_API_KEY";
// The date of your last update of your Watson Assistant
const version = "2022-01-16"; 
// Replace with your service URL if you choose another server than EU Great Britain
const serviceUrl = "https://api.eu-gb.assistant.watson.cloud.ibm.com"; 

const assistant = new AssistantV2({
  authenticator: new IamAuthenticator({ apikey }),
  serviceUrl,
  version,
});

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

const talkWithWatson = (sessionId, firstTalk) => {
  const question = firstTalk ? 'Watson > Hi, I am Watson. How can I help you ?\nYou > ' : 'You > ';;
  readline.question(`${question}`, question => {
    assistant.message(
      {
        input: { text: question },
        assistantId,
        sessionId,
      })
      .then(response => {
        console.log('Watson >', response.result.output.generic[0].text);
        talkWithWatson(sessionId, false);
      })
      .catch(err => {
        console.log(err);
      });
  }, {});
}

assistant.createSession({
    assistantId 
  }).then(res => {
    const sessionTimeOut = res.headers['x-watson-session-timeout'] * 1; // Cast to number
    console.log(`Your session will timeout in ${sessionTimeOut} seconds\n`);

    talkWithWatson(res.result.session_id, true);

    setTimeout(() => {
      readline.close();
      console.log('\n\n/!\\ Session timed out /!\\');
      exit(0);
    }, sessionTimeOut * 1000); // Convert to milliseconds
  }).catch(err => {
    console.log(err);
  });

